# NodeAPI

This repository is my starter project for building API Backend with MongoDB, Express.js, and Node.js.

This consists of Auth flow, its validation, and is written has separate Models, Routes, and Controller management. It uses ES2015 syntax and ES6 imports/exports instead of CommonJS require function.

## Usage

1. Clone the repository from Github.

2. Setup a MongoDB instance locally or a service like mlabs.

3. Run ```npm install``` or ```yarn install```

4. Rename ```.env.example``` to ```.env``` and add your configuration.

4. Run ```npm run dev``` or  ```yarn run dev``` to start the project.

5. Open NodeAPI in your preferred text editor and make something awesome.

6. Use a tool like Postman to work with this project.



### Routes

1. Make a POST request to ```/api/user/register``` to register a user. Required POST data are ```name```, ```email```, ```password```, and ```password2```. This route his handled by the RegisterController in the directory: ```app/controllers/auth/```.

2. Make a POST request to ```/api/user/login``` to login a user. Required POST data are ```email``` and ```password```. This route returns a token which can be used to access protected routes. This route his handled by the LoginController in the directory: ```app/controllers/auth/```.
